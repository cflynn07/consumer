FROM node:10.12.0-alpine
WORKDIR /home/node/app/
RUN npm install
ADD index.js index.js
ADD node_modules/ node_modules/
CMD node index.js
