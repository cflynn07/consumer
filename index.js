const http = require('http')
const request = require('request')

const procid = Math.floor(Math.random() * 1000)

const server = http.createServer((req, res) => {
  res1.statusCode = 200
  request.get(process.env.HOST_BACKEND_SERVICE + ':' + process.env.PORT_BACKEND_SERVICE, (err, res2, body) => {
    if (err) console.log(err)
    res.end('frontend service ' + procid + ' hello, backend service: ' + ((err) ? JSON.stringify(err) : body))
  })
})
server.listen(8080, '0.0.0.0', () => {
  console.log('frontend listening... ' + procid)
})
